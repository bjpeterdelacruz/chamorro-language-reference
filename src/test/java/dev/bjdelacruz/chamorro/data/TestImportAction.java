package dev.bjdelacruz.chamorro.data;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import dev.bjdelacruz.chamorro.common.AppConstants;
import org.apache.commons.lang3.Conversion;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for {@link ImportAction}.
 * 
 * @author BJ Peter DeLaCruz
 */
class TestImportAction {

  private Path file;

  /**
   * Creates a file for the tests.
   * 
   * @throws IOException If there are problems creating the file.
   */
  @BeforeEach
  void setup() throws IOException {
    file = Files.createTempFile(null, null);
  }

  /**
   * Deletes the file.
   * 
   * @throws Exception If there are problems deleting the file.
   */
  @AfterEach
  void tearDown() throws Exception {
    Files.delete(file);
  }

  /**
   * Tests whether the magic cookie is written correctly to the file.
   *
   * @throws Exception If there are problems reading from and writing to the file.
   */
  @Test
  void testIsValidFileWithValidFile() throws Exception {
    try (var os = new ObjectOutputStream(new FileOutputStream(file.toFile()))) {
      var dst = new byte[4];
      os.write(Conversion.intToByteArray(AppConstants.MAGIC_COOKIE, 0, dst, 0, 4));
    }

    assertThat(ImportAction.importEntries(file.toFile())).isEmpty();
  }

  /**
   * Tests whether the magic cookie is written correctly to the file.
   * 
   * @throws Exception If there are problems reading from and writing to the file.
   */
  @Test
  void testIsValidFileWithInvalidFile() throws Exception {
    try (var os = new ObjectOutputStream(new FileOutputStream(file.toFile()))) {
      var dst = new byte[4];
      os.write(Conversion.intToByteArray(2015_03_03, 0, dst, 0, 4));
    }

    assertThatExceptionOfType(IOException.class).isThrownBy(() -> ImportAction.importEntries(file.toFile()))
            .withMessageContaining("Invalid file: magic cookie does not match.");
  }

  /**
   * Tests whether the magic cookie is written correctly to the file.
   *
   * @throws Exception If there are problems reading from and writing to the file.
   */
  @Test
  void testImportEntries() throws Exception {
    var entry1 =
        new ChamorroEntry("a'abang", "Type of plant--eugenia reinwardtiana (Beach Cherry). "
            + "A hardwood tree formerly used to make bullcarts.", "");

    var entry2 =
        new ChamorroEntry("a'abang", "Eugenia reinwardtiana (Beach Cherry). "
            + "A hardwood tree formerly used to make bullcarts.", "A type of plant.");

    var expected = List.of(entry1, entry2);

    try (var os = new ObjectOutputStream(new FileOutputStream(file.toFile()))) {
      var dst = new byte[4];
      os.write(Conversion.intToByteArray(AppConstants.MAGIC_COOKIE, 0, dst, 0, 4));
      os.writeObject(entry1);
      os.writeObject(entry2);
    }

    assertThat(ImportAction.importEntries(file.toFile())).isEqualTo(expected);
  }

}
