package dev.bjdelacruz.chamorro.data;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class TestDataOperationsImpl {
    private static final class InMemoryDatabaseManager implements DatabaseManager {

        private final List<String[]> data = new ArrayList<>();

        @Override
        public List<String[]> get() {
            return data;
        }

        @Override
        public void insert(List<String> values) {
            data.add(values.toArray(new String[0]));
        }

        @Override
        public void update(List<String> oldValues, List<String> newValues) {
            for (var idx = 0; idx < data.size(); idx++) {
                if (Arrays.asList(data.get(idx)).equals(oldValues)) {
                    data.set(idx, newValues.toArray(new String[0]));
                    return;
                }
            }
        }

        @Override
        public void delete(List<String> values) {
            for (var idx = 0; idx < data.size(); idx++) {
                if (Arrays.asList(data.get(idx)).equals(values)) {
                    data.remove(idx);
                    return;
                }
            }
        }

        @Override
        public void deleteAll() {
            data.clear();
        }
    }

    @Test
    void testInit() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        dbManager.insert(List.of("test1", "test2", "test3"));

        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(1);
    }

    @Test
    void testAdd() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> dataOperations.add(null));

        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).isEmpty();

        assertThat(dataOperations.find("testing1", false)).isEmpty();
        dataOperations.add(new ChamorroEntry("testing1", "foo", "bar"));
        assertThat(dataOperations.find("testing1", false)).hasSize(1);
        assertThat(dataOperations.getEntries()).hasSize(1);

        dataOperations.add(new ChamorroEntry("testing1", "foo", "bar"));
        assertThat(dataOperations.find("testing1", false)).hasSize(1);
        dataOperations.add(new ChamorroEntry("testing1", "bar", "foo"));
        assertThat(dataOperations.find("testing1", false)).hasSize(2);
        dataOperations.add(new ChamorroEntry("foo", "testing1", "bar"));
        assertThat(dataOperations.find("testing1", false)).hasSize(3);
        dataOperations.add(new ChamorroEntry("bar", "testing1", "foo"));
        assertThat(dataOperations.find("testing1", false)).hasSize(4);
    }

    @Test
    void testRemove() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> dataOperations.remove(null));

        dbManager.insert(List.of("testing1", "foo", "bar"));
        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(1);

        assertThat(dataOperations.find("testing1", false)).hasSize(1);
        dataOperations.remove(new ChamorroEntry("testing1", "foo", "bar"));
        assertThat(dataOperations.find("testing1", false)).isEmpty();
        assertThat(dataOperations.getEntries()).isEmpty();
    }

    @Test
    void testUpdate() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() ->
                dataOperations.update(null, new ChamorroEntry("test1", "test2", "test3")));
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() ->
                dataOperations.update(new ChamorroEntry("test1", "test2", "test3"), null));
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() ->
                dataOperations.update(
                        new ChamorroEntry("test1", "test2", "test3"),
                        new ChamorroEntry("test4", "test5", "test6")));

        dbManager.insert(List.of("testing1", "foo", "bar"));
        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(1);

        assertThat(dataOperations.find("testing1", false)).hasSize(1);
        assertThat(dataOperations.find("testing2", false)).isEmpty();
        dataOperations.update(new ChamorroEntry("testing1", "foo", "bar"),
                new ChamorroEntry("testing2", "fizz", "buzz"));
        assertThat(dataOperations.find("testing1", false)).isEmpty();
        assertThat(dataOperations.find("testing2", false)).hasSize(1);
    }

    @Test
    void testFind() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        assertThat(dataOperations.find("test", true)).isEmpty();
        assertThat(dataOperations.find("testing", false)).isEmpty();

        dbManager.insert(List.of("testing1", "foo", "bar"));
        dbManager.insert(List.of("foo", "testing2", "bar"));
        dbManager.insert(List.of("foo", "bar", "3testing4"));
        dbManager.insert(List.of("fizz", "buzz", "fizzbuzz"));

        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(4);
        assertThat(dataOperations.find("test", true)).hasSize(2);
        assertThat(dataOperations.find("test", false)).hasSize(3);
        assertThat(dataOperations.find(null, true)).hasSize(4);
        assertThat(dataOperations.find("", true)).hasSize(4);
    }

    @Test
    void testRemoveAll() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        dbManager.insert(List.of("test1", "test2", "test3"));

        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(1);
        dataOperations.removeAll();
        assertThat(dataOperations.getEntries()).isEmpty();
    }

    @Test
    void testHasEntry() {
        var dbManager = new InMemoryDatabaseManager();
        var dataOperations = new DataOperationsImpl(dbManager);

        dbManager.insert(List.of("test1", "test2", "test3"));

        assertThat(dataOperations.getEntries()).isEmpty();
        dataOperations.init();
        assertThat(dataOperations.getEntries()).hasSize(1);
        assertThat(dataOperations.hasEntry(new ChamorroEntry("test1", "test2", "test3"))).isTrue();
        assertThat(dataOperations.hasEntry(new ChamorroEntry("test4", "test5", "test6"))).isFalse();
        assertThat(dataOperations.hasEntry(null)).isFalse();
    }
}
