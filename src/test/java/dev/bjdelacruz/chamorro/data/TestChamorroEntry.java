/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * JUnit test for the {@link ChamorroEntry} class.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
class TestChamorroEntry {

  private Path file;

  /**
   * Creates a file for the tests.
   * 
   * @throws IOException If there are problems creating the file.
   */
  @BeforeEach
  void setup() throws IOException {
    file = Files.createTempFile(null, null);
  }

  /**
   * Deletes the file.
   * 
   * @throws IOException If there are problems deleting the file.
   */
  @AfterEach
  void tearDown() throws IOException {
    Files.delete(file);
  }

  /**
   * Tests the object deserialization process.
   *
   * @throws ClassNotFoundException If there are problems during the deserialization process.
   * @throws IOException If there are problems reading from and writing to the file.
   */
  @Test
  void testCreateInstance() throws ClassNotFoundException, IOException {
    var entry =
        new ChamorroEntry("a'abang", "Eugenia reinwardtiana (Beach Cherry). "
            + "A hardwood tree formerly used to make bullcarts.", "A type of plant.");
    writeObject(entry);
    assertThat(readObject()).isEqualTo(entry);
  }

  /**
   * Writes the given entry to the file.
   * 
   * @param entry The entry to write.
   * @throws IOException If there are problems writing to the file.
   */
  void writeObject(ChamorroEntry entry) throws IOException {
    try (var os = new ObjectOutputStream(new FileOutputStream(file.toFile()))) {
      os.writeObject(entry);
    }
  }

  /**
   * Reads an entry from the file.
   * 
   * @return An entry.
   * @throws ClassNotFoundException If there are problems during the deserialization process.
   * @throws IOException If there are problems reading from the file.
   */
  private ChamorroEntry readObject() throws ClassNotFoundException, IOException {
    try (var is = new ObjectInputStream(new FileInputStream(file.toFile()))) {
      return (ChamorroEntry) is.readObject();
    }
  }

}
