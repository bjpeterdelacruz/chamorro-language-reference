package dev.bjdelacruz.chamorro.utils;

import dev.bjdelacruz.chamorro.common.utils.ChamorroAlphabetSorter;
import dev.bjdelacruz.chamorro.data.ChamorroEntry;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * JUnit test for the {@link ChamorroAlphabetSorter} class.
 *
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
class TestChamorroAlphabetSorter {

    /**
     * Tests the {@link ChamorroAlphabetSorter#sort(List)} method.
     */
    @Test
    void testSort() {
        var unsortedWords = List.of(
                new ChamorroEntry("å", "a letter", ""),
                new ChamorroEntry("pot fabot", "Please", ""),
                new ChamorroEntry("chålan", "A street", ""),
                new ChamorroEntry("chachalamcham", "stammerer", ""),
                new ChamorroEntry("adios", "goodbye", ""),
                new ChamorroEntry("ngånga'", "duck", ""),
                new ChamorroEntry("-yi", "suffix", ""),
                new ChamorroEntry("numiru", "number", ""));
        var actual = ChamorroAlphabetSorter.sort(unsortedWords);
        assertThat(actual).isEqualTo(
                List.of(unsortedWords.get(4), unsortedWords.getFirst(), unsortedWords.get(3), unsortedWords.get(2),
                        unsortedWords.get(7), unsortedWords.get(5), unsortedWords.get(1), unsortedWords.get(6)));
    }
}
