/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.main;

import java.awt.Dimension;
import java.awt.event.KeyListener;
import java.util.Objects;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.chamorro.common.i18n.LangUtils;
import dev.bjdelacruz.chamorro.data.H2DatabaseManagerImpl;
import dev.bjdelacruz.chamorro.table.EntryTableModel;

import net.miginfocom.swing.MigLayout;

/**
 * A panel that contains text fields for adding and updating entries in the table.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
class EntryPanel extends JPanel {

  private final JTextField chamorroTextField;
  private final JTextArea englishTextArea;
  private final JTextArea notesTextArea;
  private final JTable table;

  /**
   * Creates a panel that contains text fields for adding and updating entries.
   * 
   * @param table The table.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  EntryPanel(JTable table) {
    Objects.requireNonNull(table);
    this.table = table;

    table.getSelectionModel().addListSelectionListener(new ListSelectionListenerImpl());

    JPanel chamorroPanel, englishPanel, notesPanel;
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      setLayout(new MigLayout("", "0[grow,fill]0", "0[]0[]0[]0"));
      chamorroPanel = new JPanel(new MigLayout("", "[][grow,fill]0", "0[]0"));
      englishPanel = new JPanel(new MigLayout("", "[][grow,fill]0", "[]"));
      notesPanel = new JPanel(new MigLayout("", "[][grow,fill]0", "[]"));
    }
    else {
      setLayout(new MigLayout("", "[grow,fill]", "[][][]"));
      chamorroPanel = new JPanel(new MigLayout("", "[][grow,fill]", "[]"));
      englishPanel = new JPanel(new MigLayout("", "[][grow,fill]", "[]"));
      notesPanel = new JPanel(new MigLayout("", "[][grow,fill]", "[]"));
    }
    var chamorroLabel = new JLabel(LangUtils.getString("chamorro") + ": ");
    chamorroLabel.setFont(AppConstants.PANEL_FONT);
    var firstRow = "cell 0 0";
    chamorroPanel.add(chamorroLabel, firstRow);
    chamorroTextField = new JTextField();
    chamorroTextField.setFont(AppConstants.PANEL_FONT);
    chamorroTextField.setToolTipText(LangUtils.getString("chamorro_tip"));
    chamorroTextField.setDocument(new JTextFieldLimit(H2DatabaseManagerImpl.CHAMORRO_MAX_CHARS));
    var width = chamorroTextField.getPreferredSize().width;
    chamorroPanel.add(chamorroTextField, "cell 1 0");

    var englishLabel = new JLabel(LangUtils.getString("english") + ": ");
    englishLabel.setPreferredSize(chamorroLabel.getPreferredSize());
    englishLabel.setFont(AppConstants.PANEL_FONT);
    englishPanel.add(englishLabel, firstRow);
    englishTextArea = new JTextArea();
    englishTextArea.setFont(AppConstants.PANEL_FONT);
    englishTextArea.setPreferredSize(new Dimension(width, 100));
    englishTextArea.setBorder(chamorroTextField.getBorder());
    englishTextArea.setLineWrap(true);
    englishTextArea.setWrapStyleWord(true);
    englishTextArea.setName("english");
    englishTextArea.setToolTipText(LangUtils.getString("english_tip"));
    englishTextArea.setDocument(new JTextFieldLimit(H2DatabaseManagerImpl.ENGLISH_MAX_CHARS));
    englishPanel.add(englishTextArea, "cell 1 0");

    var notesLabel = new JLabel(LangUtils.getString("notes") + ": ");
    notesLabel.setPreferredSize(chamorroLabel.getPreferredSize());
    notesLabel.setFont(AppConstants.PANEL_FONT);
    notesPanel.add(notesLabel, firstRow);
    notesTextArea = new JTextArea();
    notesTextArea.setFont(AppConstants.PANEL_FONT);
    notesTextArea.setPreferredSize(new Dimension(width, 100));
    notesTextArea.setBorder(chamorroTextField.getBorder());
    notesTextArea.setLineWrap(true);
    notesTextArea.setWrapStyleWord(true);
    notesTextArea.setName("notes");
    notesTextArea.setToolTipText(LangUtils.getString("notes_tip"));
    notesTextArea.setDocument(new JTextFieldLimit(H2DatabaseManagerImpl.NOTES_MAX_CHARS));
    notesPanel.add(notesTextArea, "cell 1 0");

    add(chamorroPanel, firstRow);
    add(englishPanel, "cell 0 1");
    add(notesPanel, "cell 0 2");
  }

  /**
   * A listener that will update the text fields in this panel.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class ListSelectionListenerImpl implements ListSelectionListener {

    /** {@inheritDoc} */
    @Override
    public void valueChanged(ListSelectionEvent event) {
      var model = (EntryTableModel) table.getModel();
      var row = (table.getSelectedRows().length == 0) ? 0 : table.getSelectedRows()[0];
      var entry = model.getEntryAt(row);
      if (entry != null) {
        chamorroTextField.setText(entry.chamorro());
        englishTextArea.setText(entry.english());
        notesTextArea.setText(entry.notes());
      }
    }

  }

  /** {@inheritDoc} */
  @Override
  public void addKeyListener(KeyListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("Listener must not be null.");
    }
    chamorroTextField.addKeyListener(listener);
    englishTextArea.addKeyListener(listener);
    notesTextArea.addKeyListener(listener);
  }

  /** @return The value of the text field for entering a Chamorro word. */
  public String getChamorroTextFieldValue() {
    return chamorroTextField.getText();
  }

  /** @return The value of the text area for entering the English meaning. */
  public String getEnglishTextAreaValue() {
    return englishTextArea.getText();
  }

  /** @return The value of the text area for entering notes about an entry. */
  public String getNotesTextAreaValue() {
    return notesTextArea.getText();
  }

}
