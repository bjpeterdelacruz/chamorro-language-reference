/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.chamorro.common.i18n.LangUtils;
import dev.bjdelacruz.chamorro.data.H2DatabaseManagerImpl;

import net.miginfocom.swing.MigLayout;

/**
 * A panel that contains components that are used to search the database for entries that match a
 * specific criteria entered by the user.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
class SearchPanel extends JPanel {

  private final JTextField searchTextField;
  private final JRadioButton containsButton;
  private final JButton searchButton;
  private final JButton clearButton;

  /**
   * Creates a new SearchPanel.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  SearchPanel() {
    MigLayout outerLayout;

    JPanel textFieldPanel;
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      outerLayout = new MigLayout("", "0[grow,fill]0", "[][]");
      textFieldPanel = new JPanel(new MigLayout("", "[][grow,fill]0", "0[]0"));
    }
    else {
      outerLayout = new MigLayout("", "[grow,fill]", "[][]");
      textFieldPanel = new JPanel(new MigLayout("", "[][grow,fill]", "[]"));
    }
    setLayout(outerLayout);

    var searchLabel = new JLabel(LangUtils.getString("search_text") + ": ");
    searchLabel.setPreferredSize(new Dimension(64, 15));
    searchLabel.setFont(AppConstants.PANEL_FONT);
    textFieldPanel.add(searchLabel, "cell 0 0");

    searchTextField = new JTextField();
    searchTextField.setColumns(25);
    searchTextField.setFont(AppConstants.PANEL_FONT);
    searchTextField.setToolTipText(LangUtils.getString("search_text_tip"));
    searchTextField.setDocument(new JTextFieldLimit(H2DatabaseManagerImpl.CHAMORRO_MAX_CHARS));
    searchTextField.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent event) {
        searchButton.setEnabled(!searchTextField.getText().isEmpty());
      }

    });
    textFieldPanel.add(searchTextField, "cell 1 0");

    searchButton = new JButton(LangUtils.getString("search"));
    clearButton = new JButton(LangUtils.getString("clear_results"));

    searchButton.setEnabled(false);
    searchButton.setMnemonic(KeyEvent.VK_S);

    clearButton.setEnabled(false);
    clearButton.setMnemonic(KeyEvent.VK_C);

    searchButton.setFont(AppConstants.BUTTON_FONT);
    clearButton.setFont(AppConstants.BUTTON_FONT);

    searchButton.setToolTipText(LangUtils.getString("search_button_tip"));
    clearButton.setToolTipText(LangUtils.getString("clear_results_tip"));

    var group = new ButtonGroup();
    containsButton = new JRadioButton(LangUtils.getString("contains"));
    var startsWithButton = new JRadioButton(LangUtils.getString("starts_with"));
    containsButton.setFont(AppConstants.BUTTON_FONT);
    startsWithButton.setFont(AppConstants.BUTTON_FONT);
    containsButton.setToolTipText(LangUtils.getString("contains_tip"));
    startsWithButton.setToolTipText(LangUtils.getString("starts_with_tip"));
    group.add(containsButton);
    group.add(startsWithButton);
    containsButton.setSelected(true);

    var layout = new BorderLayout();
    JPanel leftButtonPanel, rightButtonPanel;
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      leftButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
      rightButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));

      layout.setHgap(0);
      layout.setVgap(0);
    }
    else {
      leftButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      rightButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

      layout.setHgap(5);
      layout.setVgap(5);
    }
    var buttonPanel = new JPanel(layout);
    searchButton.setPreferredSize(clearButton.getPreferredSize());

    leftButtonPanel.add(containsButton);
    leftButtonPanel.add(startsWithButton);
    rightButtonPanel.add(searchButton);
    rightButtonPanel.add(clearButton);
    buttonPanel.add(leftButtonPanel, BorderLayout.WEST);
    buttonPanel.add(rightButtonPanel, BorderLayout.EAST);

    add(textFieldPanel, "cell 0 0");
    add(buttonPanel, "cell 0 1");
  }

  /** @return The text in the Search text field. */
  String getSearchText() {
    return searchTextField.getText();
  }

  /**
   * Clears the text in the Search text field.
   */
  void clearSearchText() {
    searchTextField.setText("");
  }

  /**
   * Disables the Search button.
   */
  void disableSearchButton() {
    searchButton.setEnabled(false);
  }

  /**
   * @param enabled <code>true</code> to enable the Clear Results button, <code>false</code>
   * otherwise.
   */
  void enableClearButton(boolean enabled) {
    clearButton.setEnabled(enabled);
  }

  /**
   * Adds the given action to the Search button.
   * 
   * @param action The action to add to the Search button.
   */
  void addSearchAction(ActionListener action) {
    if (action == null) {
      throw new IllegalArgumentException("Action must not be null.");
    }
    searchButton.addActionListener(action);
  }

  /**
   * Adds the given action to the Clear Results button.
   * 
   * @param action The action to add to the Clear Results button.
   */
  void addClearAction(ActionListener action) {
    if (action == null) {
      throw new IllegalArgumentException("Action must not be null.");
    }
    clearButton.addActionListener(action);
  }

  /** @return 1 if the Contains button was selected or 2 if the Starts With button was selected. */
  int getSelectedRadioButton() {
    return containsButton.isSelected() ? 1 : 2;
  }

}
