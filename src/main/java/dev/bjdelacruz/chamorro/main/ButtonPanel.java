/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Objects;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.chamorro.common.i18n.LangUtils;
import dev.bjdelacruz.chamorro.data.ChamorroEntry;
import dev.bjdelacruz.chamorro.data.DataOperations;
import dev.bjdelacruz.chamorro.table.CustomTable;
import dev.bjdelacruz.chamorro.table.EntryTableModel;

import net.miginfocom.swing.MigLayout;

/**
 * A panel that contains buttons for adding, updating, and removing entries.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
final class ButtonPanel extends JPanel implements KeyListener {

  private final JButton addButton;
  private final JButton updateButton;
  private final JButton deleteButton;
  private final DataOperations dataOperations;
  private final EntryPanel entryPanel;
  private final JTable table;

  /**
   * Creates a new panel that contains buttons.
   * 
   * @param dataOperations The data manager.
   * @param entryPanel The panel that contains text fields for adding or updating entries.
   * @param table The table.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  ButtonPanel(DataOperations dataOperations, EntryPanel entryPanel, JTable table) {
    Objects.requireNonNull(dataOperations);
    Objects.requireNonNull(entryPanel);
    Objects.requireNonNull(table);

    this.dataOperations = dataOperations;
    this.entryPanel = entryPanel;
    this.table = table;

    addButton = new JButton(LangUtils.getString("add"));
    updateButton = new JButton(LangUtils.getString("update"));
    deleteButton = new JButton(LangUtils.getString("delete"));

    table.getSelectionModel().addListSelectionListener(_ -> {
        var entry = getSelectedEntry();
        if (dataOperations.hasEntry(entry)) {
          addButton.setEnabled(false);
          updateButton.setEnabled(true);
          deleteButton.setEnabled(true);
        }
    });

    var migLayout = new MigLayout("", "[grow,fill]", "[]");
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      migLayout = new MigLayout("", "[grow,fill]0", "0[]0");
    }

    setLayout(migLayout);
    setBorder(BorderFactory.createEmptyBorder());

    addButton.setPreferredSize(deleteButton.getPreferredSize());
    updateButton.setPreferredSize(deleteButton.getPreferredSize());

    addActionListeners();

    addButton.setEnabled(false);
    updateButton.setEnabled(false);
    deleteButton.setEnabled(false);

    addButton.setMnemonic(KeyEvent.VK_A);
    updateButton.setMnemonic(KeyEvent.VK_U);
    deleteButton.setMnemonic(KeyEvent.VK_D);

    addButton.setToolTipText(LangUtils.getString("add_tip"));
    updateButton.setToolTipText(LangUtils.getString("update_tip"));
    deleteButton.setToolTipText(LangUtils.getString("delete_tip"));

    addButton.setFont(AppConstants.BUTTON_FONT);
    updateButton.setFont(AppConstants.BUTTON_FONT);
    deleteButton.setFont(AppConstants.BUTTON_FONT);

    JPanel panel;
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      panel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));

      var preferredSize = new Dimension(122, 29);
      addButton.setPreferredSize(preferredSize);
      updateButton.setPreferredSize(preferredSize);
      deleteButton.setPreferredSize(preferredSize);
    }
    else {
      panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

      var preferredSize = new Dimension(111, 23);
      addButton.setPreferredSize(preferredSize);
      updateButton.setPreferredSize(preferredSize);
      deleteButton.setPreferredSize(preferredSize);
    }
    panel.add(addButton);
    panel.add(updateButton);
    panel.add(deleteButton);
    add(panel, "cell 0 0");
  }

  /**
   * Adds action listeners to the Add, Update, and Remove buttons.
   */
  private void addActionListeners() {
    addButton.addActionListener(_ -> {
        var entry = createEntry();

        dataOperations.add(entry);

        updateTable();
        var index = ((EntryTableModel) table.getModel()).getRowIndexOf(entry);
        table.getSelectionModel().setSelectionInterval(index, index);
        table.scrollRectToVisible(new Rectangle(table.getCellRect(index, 0, true)));
    });

    updateButton.addActionListener(_ -> {
        dataOperations.update(getSelectedEntry(), createEntry());
        updateTable();
    });

    deleteButton.addActionListener(_ -> {
        var numEntries = table.getSelectedRows().length;
        var msg = LangUtils.getString("confirm");
        if (numEntries > 1) {
          msg = LangUtils.getString("confirm_plural");
        }
        var frame = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, ButtonPanel.this);
        var value =
            JOptionPane.showConfirmDialog(frame, msg, LangUtils.getString("confirm_title"),
                JOptionPane.YES_NO_OPTION);
        if (value == JOptionPane.NO_OPTION) {
          return;
        }

        for (var row : table.getSelectedRows()) {
          dataOperations.remove(getEntryAt(row));
        }
        updateTable();
    });
  }

  /**
   * Returns the selected entry, or <code>null</code> if no entry is selected.
   * 
   * @return The selected entry, or <code>null</code> if no entry is selected.
   */
  private ChamorroEntry getSelectedEntry() {
    if (table.getSelectedRows().length == 0) {
      return null;
    }
    return ((EntryTableModel) table.getModel()).getEntryAt(table.getSelectedRows()[0]);
  }

  /**
   * Returns the entry at the given row.
   * 
   * @param row The index of the entry to retrieve.
   * @return The entry at the given row.
   */
  private ChamorroEntry getEntryAt(int row) {
    return ((EntryTableModel) table.getModel()).getEntryAt(row);
  }

  /**
   * Creates an entry from the values in the text fields.
   * 
   * @return The entry populated with values from the text fields.
   */
  private ChamorroEntry createEntry() {
    var chamorro = entryPanel.getChamorroTextFieldValue();
    var english = entryPanel.getEnglishTextAreaValue();
    var notes = entryPanel.getNotesTextAreaValue();
    return new ChamorroEntry(chamorro, english, notes);
  }

  /**
   * Updates the table model and sets the renderer for the cells in the table.
   */
  private void updateTable() {
    var row = Math.max(table.getSelectedRow(), 0);
    table.setModel(new EntryTableModel(dataOperations.getEntries()));
    ((CustomTable) table).setCellRenderers();
    table.getSelectionModel().setSelectionInterval(row, row);
  }

  /** {@inheritDoc} */
  @Override
  public void keyPressed(KeyEvent event) {
    // Do nothing.
  }

  /** {@inheritDoc} */
  @Override
  public void keyReleased(KeyEvent event) {
    var chamorro = entryPanel.getChamorroTextFieldValue();
    var english = entryPanel.getEnglishTextAreaValue();
    if (chamorro.isEmpty() || english.isEmpty()) {
      addButton.setEnabled(false);
      updateButton.setEnabled(false);
      deleteButton.setEnabled(false);
      return;
    }
    var selectedEntry = getSelectedEntry();
    if (selectedEntry == null) {
      addButton.setEnabled(true);
      updateButton.setEnabled(false);
      deleteButton.setEnabled(false);
    }
    else {
      addButton.setEnabled(!createEntry().equals(selectedEntry));
      updateButton.setEnabled(true);
      deleteButton.setEnabled(true);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void keyTyped(KeyEvent event) {
    // Do nothing.
  }

}
