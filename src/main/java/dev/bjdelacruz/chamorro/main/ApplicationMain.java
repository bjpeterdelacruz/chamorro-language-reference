/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.main;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.commons.utils.FileUtils;
import dev.bjdelacruz.commons.utils.UiUtils;

/**
 * The main entry point of this application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class ApplicationMain { // NO_UCD (unused code)

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /** <code>true</code> if this application is running on Mac OS X, <code>false</code> otherwise. */
  public static final boolean IS_RUNNING_ON_MAC_OS = UiUtils.isRunningOnMacOS();

  /* Set up the application. */
  static {
    UiUtils.useSystemLookAndFeel(LOGGER);
    LOGGER.setLevel(Level.INFO);
    if (IS_RUNNING_ON_MAC_OS) {
      System.setProperty("apple.laf.useScreenMenuBar", "true");
      UiUtils.setDefaultTableCellBorderColorMacOS();
    }
    var logDir = AppConstants.APP_DIR + "logs" + FileSystems.getDefault().getSeparator();
    var logFile = logDir + "chamorro-language-reference.log";
    try {
      Files.createDirectories(Paths.get(logDir));
      Files.createFile(Paths.get(logFile));
    }
    catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    }
    FileUtils.configureLogger(LOGGER, 1000 * 1000 * 5, 1, logFile);
  }

  /** Do not instantiate this class. */
  private ApplicationMain() {
  }

  /**
   * Constructs the main window of this application and then displays it.
   * 
   * @param args None.
   */
  public static void main(String... args) {
    new MainFrame().setVisible(true);
  }

}
