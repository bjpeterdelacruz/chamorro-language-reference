/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dev.bjdelacruz.chamorro.common.utils.ChamorroAlphabetSorter;
import dev.bjdelacruz.chamorro.data.DataOperationsImpl;
import dev.bjdelacruz.chamorro.data.H2DatabaseManagerImpl;
import dev.bjdelacruz.chamorro.common.i18n.LangUtils;
import dev.bjdelacruz.chamorro.common.utils.ThreadUtils;
import dev.bjdelacruz.chamorro.table.CustomScrollPane;
import dev.bjdelacruz.chamorro.table.CustomTable;
import dev.bjdelacruz.chamorro.table.EntryTableModel;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.Conversion;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.chamorro.data.ChamorroEntry;
import dev.bjdelacruz.chamorro.data.DataOperations;
import dev.bjdelacruz.chamorro.data.ImportAction;
import dev.bjdelacruz.commons.ui.AppAboutDialog;
import dev.bjdelacruz.commons.ui.AboutDialogConfig;
import dev.bjdelacruz.commons.ui.CopyToClipboardAction;
import dev.bjdelacruz.commons.ui.ExportAction;
import dev.bjdelacruz.commons.ui.LoadingDialog;
import dev.bjdelacruz.commons.ui.AppStatusBar;

import net.miginfocom.swing.MigLayout;

/**
 * The main window of this application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
final class MainFrame extends JFrame {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final DataOperations dataOperations;
  private final JLabel rowIndexLabel = new JLabel();
  private final JLabel statusLabel = new JLabel();
  private final JTextPane textPane = new JTextPane();
  private final JTable table;
  private final JMenuItem copyMenuItem;
  private final JPopupMenu popupMenu = new JPopupMenu();

  private final SearchPanel searchPanel = new SearchPanel();

  private static final byte[] MAGIC_COOKIE_BYTES;
  static {
    MAGIC_COOKIE_BYTES = Conversion.intToByteArray(AppConstants.MAGIC_COOKIE, 0, new byte[4], 0, 4);
  }

  /**
   * Constructs the main window of this application.
   */
  @SuppressFBWarnings("SE_BAD_FIELD_STORE")
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  MainFrame() {
    super(AppConstants.APP_NAME);

    var model = new EntryTableModel(new ArrayList<>());
    table = new CustomTable(model);

    var menuBar = new JMenuBar();

    var optionsMenu = new JMenu(LangUtils.getString("options"));
    optionsMenu.setMnemonic(KeyEvent.VK_O);
    copyMenuItem = new JMenuItem(LangUtils.getString("copy"));
    copyMenuItem.setEnabled(false);
    var copyAction = new CopyToClipboardAction<>(table);
    copyMenuItem.addActionListener(copyAction);
    var importMenuItem = new JMenuItem(LangUtils.getString("import"));
    try {
      dataOperations = new DataOperationsImpl(new H2DatabaseManagerImpl());
    }
    catch (ClassNotFoundException | SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("Unable to initialize H2 database manager: %s", e.getMessage()));
      throw new RuntimeException(e);
    }
    importMenuItem.addActionListener(new ImportAction((CustomTable) table, dataOperations));
    var exportMenuItem = new JMenuItem(LangUtils.getString("export"));
    exportMenuItem.addActionListener(new ExportAction<ChamorroEntry>(table, MAGIC_COOKIE_BYTES));
    var exitMenuItem = new JMenuItem(LangUtils.getString("exit"));
    exitMenuItem.addActionListener(_ -> dispatchEvent(
            new WindowEvent(MainFrame.this, WindowEvent.WINDOW_CLOSING)));
    optionsMenu.add(copyMenuItem);
    optionsMenu.add(importMenuItem);
    optionsMenu.add(exportMenuItem);
    optionsMenu.add(exitMenuItem);

    var helpMenu = new JMenu(LangUtils.getString("help"));
    helpMenu.setMnemonic(KeyEvent.VK_H);
    var aboutMenuItem = new JMenuItem(LangUtils.getString("about"));
    var year = LocalDate.now().getYear();
    var config = new AboutDialogConfig.AboutDialogConfigBuilder()
            .title(AppConstants.APP_NAME).version("Version 2.0")
            .copyright(String.format("Copyright &copy; 2012-%d BJ Peter Dela Cruz", year))
            .website("https://bjdelacruz.dev").email("bj.peter.delacruz@gmail.com").build();
    aboutMenuItem.addActionListener(event ->
            new AppAboutDialog(config, AppConstants.getAboutIcon(), AppConstants.getIcon(), MainFrame.this)
                    .setVisible(true));
    helpMenu.add(aboutMenuItem);

    menuBar.add(optionsMenu);
    menuBar.add(helpMenu);
    setJMenuBar(menuBar);

    var copyPopupMenuItem = new JMenuItem(LangUtils.getString("copy"));
    copyPopupMenuItem.addActionListener(copyAction);
    popupMenu.add(copyPopupMenuItem);

    table.getSelectionModel().addListSelectionListener(new ListSelectionListenerImpl());
    table.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent event) {
        if (!SwingUtilities.isRightMouseButton(event)) {
          return;
        }
        var row = table.rowAtPoint(event.getPoint());
        if (table.getSelectedRows().length == 0) {
          table.setRowSelectionInterval(row, row);
        }
        else {
          var isRowSelected = false;
          for (var selectedRow : table.getSelectedRows()) {
            if (selectedRow == row) {
              isRowSelected = true;
              break;
            }
          }
          if (!isRowSelected) {
            table.setRowSelectionInterval(row, row);
          }
        }
        popupMenu.show(table, event.getX(), event.getY());
      }

    });
    var panel = new JPanel(new MigLayout("", "[]20[]", ""));

    populateTable(model);

    var leftPanel = new JPanel(new MigLayout("", "[grow,fill]", "[][]"));
    leftPanel.setPreferredSize(new Dimension(500, 700));
    var entryPanel = new EntryPanel(table);
    leftPanel.add(entryPanel, "cell 0 0");
    var buttonPanel = new ButtonPanel(dataOperations, entryPanel, table);
    leftPanel.add(buttonPanel, "cell 0 1");
    leftPanel.add(searchPanel, "cell 0 2");
    leftPanel.add(createCopyPanel(), "cell 0 3");
    searchPanel.addSearchAction(new SearchAction());
    searchPanel.addClearAction(new ClearAction());
    entryPanel.addKeyListener(buttonPanel);
    panel.add(leftPanel);

    var scrollPane = new CustomScrollPane(table);
    scrollPane.setPreferredSize(new Dimension(600, 700));
    panel.add(scrollPane);

    var mainScrollPane = new JScrollPane();
    mainScrollPane.getVerticalScrollBar().setUnitIncrement(8);
    var centerPanel = new JPanel(new BorderLayout());
    centerPanel.add(panel, BorderLayout.CENTER);
    mainScrollPane.setViewportView(centerPanel);

    var statusBar = new AppStatusBar();
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      statusBar.setBackground(SystemColor.WHITE);
    }
    rowIndexLabel.setFont(AppConstants.PANEL_FONT);
    statusLabel.setFont(AppConstants.PANEL_FONT);
    statusBar.addComponent(rowIndexLabel, BorderLayout.WEST);
    statusBar.addComponent(statusLabel, BorderLayout.EAST);

    setLayout(new BorderLayout());
    add(mainScrollPane, BorderLayout.CENTER);
    add(statusBar, BorderLayout.SOUTH);

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setIconImage(AppConstants.getIcon());

    var toolkit = Toolkit.getDefaultToolkit();
    var screenSize = toolkit.getScreenSize();
    var shouldResize = false;
    var width = 1152;
    if (screenSize.width < width) {
      width = screenSize.width - 100;
      shouldResize = true;
    }
    var height = 799;
    if (screenSize.height < height) {
      height = screenSize.height - 100;
      shouldResize = true;
    }
    if (shouldResize) {
      setPreferredSize(new Dimension(width, height));
    }
    pack();

    setLocationRelativeTo(null);
  }

  /**
   * Creates the panel that will contain the Copy to Clipboard hyperlink.
   * 
   * @return A panel that contains the Copy to Clipboard hyperlink.
   */
  private JPanel createCopyPanel() {
    JPanel copyPanel;
    if (ApplicationMain.IS_RUNNING_ON_MAC_OS) {
      copyPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
    }
    else {
      copyPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    }
    var panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    var msg = """
            <html><span style="font: Arial; font-size: 13pt"><a href="" style="text-decoration: none;">""";
    msg += LangUtils.getString("copy") + "</a></span></html>";
    textPane.setOpaque(false);
    textPane.setContentType("text/html");
    textPane.setText(msg);
    textPane.setEditable(false);
    textPane.setEnabled(false);
    textPane.addHyperlinkListener(new HyperlinkListenerImpl());
    textPane.setToolTipText(LangUtils.getString("copy_tip"));
    copyPanel.add(textPane);
    panel.add(copyPanel);
    return panel;
  }

  /**
   * A listener that will copy the contents of the selected rows in the table to the clipboard when
   * the user clicks on the Copy to Clipboard hyperlink.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class HyperlinkListenerImpl implements HyperlinkListener {

    private final CopyToClipboardAction<ChamorroEntry> action = new CopyToClipboardAction<>(table, "");

    /** {@inheritDoc} */
    @Override
    public void hyperlinkUpdate(HyperlinkEvent event) {
      if (HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType())) {
        action.actionPerformed(null);
      }
    }

  }

  /**
   * A listener that will enable the text pane that contains the Copy to Clipboard link when the
   * user clicks on at least one row in the table.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class ListSelectionListenerImpl implements ListSelectionListener {

    /** {@inheritDoc} */
    @Override
    public void valueChanged(ListSelectionEvent event) {
      var enabled = table.getSelectedRows().length > 0;
      textPane.setEnabled(enabled);
      copyMenuItem.setEnabled(enabled);
      if (enabled) {
        var currentRow = NumberFormat.getInstance().format(table.getSelectedRow() + 1);
        var numRows = NumberFormat.getInstance().format(table.getRowCount());
        var msg = currentRow + " / " + numRows;
        rowIndexLabel.setText("  " + msg);
      }
    }

  }

  /**
   * Populates the given table model with data.
   * 
   * @param model The model to populate with data.
   */
  private void populateTable(EntryTableModel model) {
    if (model == null) {
      throw new IllegalArgumentException("Model must not be null.");
    }
    ThreadUtils.execute(() -> {

        var dialog =
            new LoadingDialog(AppConstants.APP_NAME, LangUtils.getString("init_msg"), 300, 75,
                AppConstants.PANEL_FONT, AppConstants.getIcon());
        SwingUtilities.invokeLater(() -> {
            dialog.pack();
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        });

        dataOperations.init();

        var entries = dataOperations.getEntries();

        SwingUtilities.invokeLater(() -> {
            dialog.dispose();
            setVisible(true);
            entries.forEach(model::addEntry);
            var num = NumberFormat.getInstance().format(entries.size());
            var msg = "Successfully loaded " + num + " entries.";
            if (entries.size() == 1) {
              msg = msg.replace("entries", "entry");
            }
            statusLabel.setText(msg + "  ");
        });

    });
  }

  /**
   * An action that will search the database for entries that contain the text that was entered by
   * the user and show the search results in the table.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class SearchAction implements ActionListener {

    /** {@inheritDoc} */
    @Override
    public void actionPerformed(ActionEvent event) {
      var searchString = searchPanel.getSearchText();
      var startsWith = searchPanel.getSelectedRadioButton() == 2;
      var entries = ChamorroAlphabetSorter.sort(dataOperations.find(searchString, startsWith));
      var model = new EntryTableModel(entries);
      table.setModel(model);
      table.clearSelection();
      ((CustomTable) table).setCellRenderers();
      model.fireTableDataChanged();
      searchPanel.enableClearButton(true);
      var number = NumberFormat.getInstance().format(entries.size());
      var msg = String.format("Found %s %s that %s ", number,
              entries.size() == 1 ? "entry" : "entries", entries.size() == 1 ? "matches" : "match");
      statusLabel.setText(String.format("<html>%s<b>%s</b>.</html>", msg, searchString));
      LOGGER.log(Level.INFO, String.format("%s\"%s\".", msg, searchString));
    }

  }

  /**
   * An action that will clear the search results and add all the entries in the database back to
   * the table.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private final class ClearAction implements ActionListener {

    /** {@inheritDoc} */
    @Override
    public void actionPerformed(ActionEvent event) {
      var model = new EntryTableModel(dataOperations.getEntries());
      table.setModel(model);
      table.clearSelection();
      ((CustomTable) table).setCellRenderers();
      model.fireTableDataChanged();
      statusLabel.setText("");
      searchPanel.clearSearchText();
      searchPanel.disableSearchButton();
      searchPanel.enableClearButton(false);
    }

  }

}
