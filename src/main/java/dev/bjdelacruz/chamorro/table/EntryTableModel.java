/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.bjdelacruz.chamorro.common.utils.ChamorroAlphabetSorter;
import dev.bjdelacruz.chamorro.data.ChamorroEntry;
import dev.bjdelacruz.commons.ui.CustomTableModel;

/**
 * A table model for displaying Chamorro words, their English meanings, and user's notes.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class EntryTableModel extends CustomTableModel<ChamorroEntry> {

  private static final int NUM_COLUMNS = 3;

  private final List<ChamorroEntry> entries;

  /**
   * Constructs a new table model using the given list of entries. The list is sorted in
   * alphabetical order in this constructor.
   * 
   * @param entries The list of entries to use in this table model.
   */
  public EntryTableModel(List<ChamorroEntry> entries) {
    Objects.requireNonNull(entries);
    this.entries = ChamorroAlphabetSorter.sort(new ArrayList<>(entries));
  }

  /** {@inheritDoc} */
  @Override
  public int getColumnCount() {
    return NUM_COLUMNS;
  }

  /** {@inheritDoc} */
  @Override
  public int getRowCount() {
    return entries.size();
  }

  /** {@inheritDoc} */
  @Override
  public Object getValueAt(int row, int column) {
    return switch (column) {
      case 0 -> entries.get(row).chamorro();
      case 1 -> entries.get(row).english();
      case 2 -> entries.get(row).notes();
      default -> throw new IllegalArgumentException("Invalid column index: " + column);
    };
  }

  /** {@inheritDoc} */
  @Override
  public String getColumnName(int column) {
    return switch (column) {
      case 0 -> "Chamorro";
      case 1 -> "English";
      case 2 -> "Notes";
      default -> throw new IllegalArgumentException("Invalid column index: " + column);
    };
  }

  /** {@inheritDoc} */
  @Override
  public boolean isCellEditable(int row, int column) {
    return false;
  }

  /** {@inheritDoc} */
  @Override
  public void addEntry(ChamorroEntry entry) {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    entries.add(entry);
    fireTableDataChanged();
  }

  /** {@inheritDoc} */
  @Override
  public ChamorroEntry getEntryAt(int row) {
    if (row < 0 || row >= entries.size()) {
      return null;
    }
    return entries.get(row);
  }

  /** {@inheritDoc} */
  @Override
  public int getRowIndexOf(ChamorroEntry entry) {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    return entries.indexOf(entry);
  }

}
