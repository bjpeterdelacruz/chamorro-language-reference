/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;

import dev.bjdelacruz.chamorro.common.AppConstants;

/**
 * A customized table for displaying Chamorro words, their English meanings, and user's notes.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class CustomTable extends JTable {

  private static final Color PANEL_COLOR = new JPanel().getBackground();

  /**
   * Constructs a new table with the given table model and then customizes it.
   * 
   * @param model The table model for this table.
   */
  public CustomTable(EntryTableModel model) {
    super(model);

    tableHeader.setDefaultRenderer(new TableHeaderRenderer());
    var preferredSize = tableHeader.getPreferredSize();
    tableHeader.setPreferredSize(new Dimension(preferredSize.width, 32));
    setCellRenderers();
    setRowHeight(20);
    setShowVerticalLines(false);
    setShowHorizontalLines(true);
    getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
  }

  /**
   * A renderer for the table header.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private static final class TableHeaderRenderer implements TableCellRenderer {

    /** {@inheritDoc} */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      var label = new JLabel(value.toString());
      label.setFont(AppConstants.TABLE_CELL_FONT);
      label.setHorizontalAlignment(JLabel.CENTER);
      label.setOpaque(true);
      label.setBackground(PANEL_COLOR);
      return label;
    }

  }

  /**
   * A renderer for the table cells.
   * 
   * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
   */
  private static final class CellRenderer implements TableCellRenderer {

    /** {@inheritDoc} */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      var label = new JLabel(value.toString());
      label.setFont(AppConstants.TABLE_CELL_FONT);
      label.setOpaque(true);
      if (isSelected) {
        label.setBackground(table.getSelectionBackground());
        label.setForeground(table.getSelectionForeground());
      }
      else {
        label.setBackground(table.getBackground());
        label.setForeground(table.getForeground());
      }
      return label;
    }

  }

  /**
   * Sets the renderer for the cells in this table.
   */
  public void setCellRenderers() {
    var renderer = new CellRenderer();
    columnModel.getColumn(0).setCellRenderer(renderer);
    columnModel.getColumn(1).setCellRenderer(renderer);
    columnModel.getColumn(2).setCellRenderer(renderer);
  }

}
