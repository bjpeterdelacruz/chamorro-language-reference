/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.table;

import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * A customized scroll pane for a JTable.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public class CustomScrollPane extends JScrollPane {

  /**
   * Constructs the scroll pane for the given table.
   * 
   * @param table The table to put in this scroll pane.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public CustomScrollPane(JTable table) {
    super(table);

    setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    viewport.setOpaque(false);
    setBackground(Color.WHITE);
  }

}
