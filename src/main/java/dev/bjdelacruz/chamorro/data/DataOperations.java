/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import java.util.List;

/**
 * Contains operations for adding, updating, removing, and finding entries in the database.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public interface DataOperations {

  /**
   * Initializes the underlying database.
   */
  void init();

  /**
   * Adds the given entry to the database.
   * 
   * @param entry The entry to add.
   */
  void add(ChamorroEntry entry);

  /**
   * Removes the given entry from the database.
   * 
   * @param entry The entry to remove.
   */
  void remove(ChamorroEntry entry);

  /**
   * Updates an entry in the database.
   * 
   * @param oldEntry The old entry to update.
   * @param newEntry The new entry to insert.
   */
  void update(ChamorroEntry oldEntry, ChamorroEntry newEntry);

  /**
   * Returns a list of entries in the database that match the given criteria.
   * 
   * @param strToFind The string to use in the search.
   * @param startsWith <code>true</code> to find entries that start with the given regex,
   * <code>false</code> to find entries that contain the given regex.
   * @return The list of entries that match the given criteria.
   */
  List<ChamorroEntry> find(String strToFind, boolean startsWith);

  /**
   * Removes all the entries from the database.
   */
  void removeAll();

  /** @return A sorted list containing all entries in the database. */
  List<ChamorroEntry> getEntries();

  /**
   * Returns true if the map contains the given entry, false otherwise.
   *
   * @param entry The entry to find in the map.
   * @return True if the map contains the given entry, false otherwise.
   */
  boolean hasEntry(ChamorroEntry entry);
}
