/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * A class that represents a definition entry for a Chamorro word.
 *
 * @param chamorro The Chamorro word.
 * @param english  The English meaning.
 * @param notes    The user's notes.
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public record ChamorroEntry(String chamorro, String english, String notes) implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  /**
   * Creates a new entry.
   */
  public ChamorroEntry {
    if (chamorro == null || chamorro.isEmpty()) {
      throw new IllegalArgumentException("Chamorro word must not be null or an empty string.");
    }
    if (english == null || english.isEmpty()) {
      throw new IllegalArgumentException("English word must not be null or an empty string.");
    }
    if (notes == null) {
      throw new IllegalArgumentException("Notes must not be null.");
    }

    chamorro = chamorro.trim();
    english = english.trim();
    notes = notes.trim();
  }

  /**
   * @return A list of values in this entry.
   */
  public List<String> getValues() {
    return Arrays.asList(chamorro, english, notes);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return chamorro + " | " + english + (notes.isEmpty() ? "" : " | " + notes);
  }

}
