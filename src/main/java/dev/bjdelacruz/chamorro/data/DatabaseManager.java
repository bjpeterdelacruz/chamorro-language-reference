/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import java.util.List;

/**
 * Contains methods for issuing SQL commands to the underlying database.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
interface DatabaseManager {

  /**
   * Gets all the rows in the table.
   * 
   * @return A list of all rows in the table.
   */
  List<String[]> get();

  /**
   * Inserts a row in the table with the given values.
   * 
   * @param values The values to insert.
   */
  void insert(List<String> values);

  /**
   * Updates the given row with new values.
   * 
   * @param oldValues The old values to replace.
   * @param newValues The new values to insert.
   */
  void update(List<String> oldValues, List<String> newValues);

  /**
   * Deletes a row in the table with the given values.
   * 
   * @param values The values to delete.
   */
  void delete(List<String> values);

  /**
   * Deletes all rows in the table.
   */
  void deleteAll();

}
