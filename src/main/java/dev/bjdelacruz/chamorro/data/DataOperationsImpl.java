/*******************************************************************************
 * Copyright (C) 2012-2023 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import dev.bjdelacruz.chamorro.common.utils.ChamorroAlphabetSorter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A manager that contains a map of entries that exist in the database. The map will <b>always</b>
 * be in sync with the data in the database.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public class DataOperationsImpl implements DataOperations {

  private final Map<Integer, ChamorroEntry> entriesMap = new HashMap<>();

  private final DatabaseManager databaseManager;

  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public DataOperationsImpl(DatabaseManager databaseManager) {
    this.databaseManager = databaseManager;
  }

  /** {@inheritDoc} */
  @Override
  public void init() {
    var entries = databaseManager.get();
    for (var entry : entries) {
      var chamorro = entry[0].trim();
      var english = entry[1].trim();
      var notes = entry[2].trim();
      var ce = new ChamorroEntry(chamorro, english, notes);
      entriesMap.put(ce.hashCode(), ce);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void add(ChamorroEntry entry) {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    if (!hasEntry(entry)) {
      databaseManager.insert(entry.getValues());
      entriesMap.put(entry.hashCode(), entry);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void remove(ChamorroEntry entry) {
    if (entry == null) {
      throw new IllegalArgumentException("Entry must not be null.");
    }
    databaseManager.delete(entry.getValues());
    entriesMap.remove(entry.hashCode());
  }

  /** {@inheritDoc} */
  @Override
  public void update(ChamorroEntry oldEntry, ChamorroEntry newEntry) {
    if (oldEntry == null) {
      throw new IllegalArgumentException("Old entry must not be null.");
    }
    if (newEntry == null) {
      throw new IllegalArgumentException("New entry must not be null.");
    }
    var v = entriesMap.remove(oldEntry.hashCode());
    if (v == null) {
      throw new IllegalArgumentException(oldEntry.chamorro() + " does not exist in map.");
    }
    databaseManager.update(oldEntry.getValues(), newEntry.getValues());
    entriesMap.put(newEntry.hashCode(), newEntry);
  }

  /** {@inheritDoc} */
  @Override
  public List<ChamorroEntry> find(String strToFind, boolean startsWith) {
    if (strToFind == null || strToFind.isEmpty()) {
      return new ArrayList<>(entriesMap.values());
    }

    var entries = new ArrayList<ChamorroEntry>();
    for (var entry : entriesMap.values()) {
      var match = entry.getValues().stream().filter(value -> find(value, strToFind, startsWith)).findAny();
      if (match.isPresent()) {
        entries.add(entry);
      }
    }
    return entries;
  }

  /**
   * Compares two strings: one string either starts with or contains another.
   * 
   * @param string The string being compared.
   * @param strToFind The string to find.
   * @param startsWith True if the comparison should only check if <code>string</code> starts with <code>regex</code>,
   * false if the comparison should only check if <code>string</code> contains <code>regex</code>
   * @return True if a match was found, false otherwise.
   */
  private static boolean find(String string, String strToFind, boolean startsWith) {
    var str1 = string.toLowerCase(Locale.getDefault());
    var str2 = strToFind.toLowerCase(Locale.getDefault());
    return startsWith ? str1.startsWith(str2) : str1.contains(str2);
  }

  /** {@inheritDoc} */
  @Override
  public void removeAll() {
    databaseManager.deleteAll();
    entriesMap.clear();
  }

  /** {@inheritDoc} */
  @Override
  public List<ChamorroEntry> getEntries() {
    return ChamorroAlphabetSorter.sort(new ArrayList<>(entriesMap.values()));
  }

  /** {@inheritDoc} */
  @Override
  public boolean hasEntry(ChamorroEntry entry) {
    return entry != null && entriesMap.containsKey(entry.hashCode());
  }
}
