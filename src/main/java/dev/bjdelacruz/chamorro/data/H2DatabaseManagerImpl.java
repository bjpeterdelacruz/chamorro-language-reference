/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import dev.bjdelacruz.chamorro.common.utils.DbUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A database manager that uses H2 as the underlying database.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class H2DatabaseManagerImpl implements DatabaseManager {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /** The maximum number of characters for the Chamorro field. */
  public static final int CHAMORRO_MAX_CHARS = 100;

  /** The maximum number of characters for the English field. */
  public static final int ENGLISH_MAX_CHARS = 1000;

  /** The maximum number of characters for the Notes field. */
  public static final int NOTES_MAX_CHARS = 1000;

  static final String INSERT_ENTRY = "INSERT INTO DICTIONARY (CHAMORRO, ENGLISH, NOTES) VALUES (?, ?, ?)";

  static final String DELETE_ENTRY = "DELETE FROM DICTIONARY WHERE CHAMORRO = ? AND ENGLISH = ? AND NOTES = ?";

  static final String DELETE_ALL = "DELETE FROM DICTIONARY";

  static final String SELECT_ALL = "SELECT * FROM DICTIONARY";

  static final String UPDATE_ENTRY = "UPDATE DICTIONARY SET CHAMORRO = ?, ENGLISH = ?, NOTES = ? WHERE CHAMORRO = ? AND ENGLISH = ? AND NOTES = ?";

  private final Connection connection;

  /**
   * Creates a new database manager.
   *
   * @throws ClassNotFoundException If there are problems loading the JDBC driver.
   * @throws SQLException If there were problems connecting to the database.
   */
  public H2DatabaseManagerImpl() throws ClassNotFoundException, SQLException {
    Class.forName("org.h2.Driver");

    var username = DbUtils.getUsername();
    var password = DbUtils.getPassword();
    connection = DriverManager.getConnection("jdbc:h2:./db/data;user=" + username + ";password=" + password, username, password);
    LOGGER.log(Level.INFO, "Connection to database established.");
  }

  /** {@inheritDoc} */
  @Override
  public List<String[]> get() {
    var entries = new ArrayList<String[]>();
    try {
      try (var statement = connection.prepareStatement(SELECT_ALL);
           var rs = statement.executeQuery()) {
        while (rs.next()) {
          var chamorro = rs.getString("CHAMORRO");
          var english = rs.getString("ENGLISH");
          var notes = rs.getString("NOTES");
          entries.add(Arrays.asList(chamorro, english, notes).toArray(new String[3]));
        }
      }
    }
    catch (SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("An error occurred while querying the database: %s", e));
    }
    return entries;
  }

  /** {@inheritDoc} */
  @Override
  public void insert(List<String> values) {
    try (var statement = connection.prepareStatement(INSERT_ENTRY)) {
      for (var index = 0; index < values.size(); index++) {
        statement.setString(index + 1, values.get(index));
      }
      statement.executeUpdate();
      LOGGER.log(Level.INFO, "Successfully added " + values + " to the database.");
    }
    catch (SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("An error occurred while insert into the database: %s", e));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void update(List<String> oldValues, List<String> newValues) {
    try (var statement = connection.prepareStatement(UPDATE_ENTRY)) {
      var index = 0;
      for (var pos = 0; pos < oldValues.size(); pos++, index++) {
        statement.setString(index + 1, oldValues.get(pos));
      }
      for (var pos = 0; pos < newValues.size(); pos++, index++) {
        statement.setString(index + 1, newValues.get(pos));
      }
      statement.executeUpdate();
      var msg = "Successfully updated row in the database. Old values: " + oldValues + " - New Values: " + newValues;
      LOGGER.log(Level.INFO, msg);
    }
    catch (SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("An error occurred while update the database: %s", e));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void delete(List<String> values) {
    try (var statement = connection.prepareStatement(DELETE_ENTRY)) {
      for (var index = 0; index < values.size(); index++) {
        statement.setString(index + 1, values.get(index));
      }
      statement.executeUpdate();
      LOGGER.log(Level.INFO, "Successfully removed " + values + " from the database.");
    }
    catch (SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("An error occurred while delete from the database: %s", e));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void deleteAll() {
    try (var statement = connection.prepareStatement(DELETE_ALL)) {
      statement.executeUpdate();
      LOGGER.log(Level.INFO, "Successfully deleted all rows in the database.");
    }
    catch (SQLException e) {
      LOGGER.log(Level.SEVERE, String.format("An error occurred while delete from the database: %s", e));
    }
  }

}
