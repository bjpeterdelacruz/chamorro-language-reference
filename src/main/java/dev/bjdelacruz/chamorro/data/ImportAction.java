/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.data;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import dev.bjdelacruz.chamorro.common.utils.ChamorroAlphabetSorter;
import dev.bjdelacruz.chamorro.common.i18n.LangUtils;
import dev.bjdelacruz.chamorro.common.utils.ThreadUtils;
import dev.bjdelacruz.chamorro.table.CustomTable;
import dev.bjdelacruz.chamorro.table.EntryTableModel;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.Conversion;

import dev.bjdelacruz.chamorro.common.AppConstants;
import dev.bjdelacruz.commons.ui.LoadingDialog;
import dev.bjdelacruz.commons.utils.UiUtils;

/**
 * An action that will import data from a file and populate the table with entries stored in it.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class ImportAction implements ActionListener {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final CustomTable table;
  private final DataOperations dataOperations;

  /**
   * Creates a new ImportAction.
   * 
   * @param table The table that will be populated with data from the imported file.
   * @param dataOperations The data manager that contains the map of entries that will also be
   * populated with data from the imported file.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public ImportAction(CustomTable table, DataOperations dataOperations) {
      Objects.requireNonNull(table);
      Objects.requireNonNull(dataOperations);

      this.table = table;
      this.dataOperations = dataOperations;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    var chooser = new JFileChooser();
    chooser.setSelectedFile(new File("data.dat"));
    chooser.setFileFilter(new FileFilter() {

      @Override
      public boolean accept(File file) {
        return file.getName().toLowerCase(Locale.getDefault()).endsWith("dat");
      }

      @Override
      public String getDescription() {
        return "Data file (*.dat)";
      }

    });
    var returnValue = chooser.showOpenDialog(null);
    if (returnValue != JFileChooser.APPROVE_OPTION) {
      return;
    }

    try {
      var entries = importEntries(chooser.getSelectedFile());

      ThreadUtils.execute(() -> {

          var appName = AppConstants.APP_NAME;
          var dialog = new LoadingDialog(appName, LangUtils.getString("import_msg"), 250, 75, AppConstants.PANEL_FONT, AppConstants.getIcon());
          SwingUtilities.invokeLater(() -> {
              dialog.pack();
              dialog.setLocationRelativeTo(null);
              dialog.setVisible(true);
          });
          dataOperations.removeAll();

          entries.forEach(dataOperations::add);

          SwingUtilities.invokeLater(() -> {
              dialog.dispose();
              var newTableModel = new EntryTableModel(entries);
              table.setModel(newTableModel);
              table.clearSelection();
              table.setCellRenderers();
              newTableModel.fireTableDataChanged();

              LOGGER.log(Level.INFO, LangUtils.getString("success"));
              var frame = SwingUtilities.getAncestorOfClass(JFrame.class, table);
              JOptionPane.showMessageDialog(frame, LangUtils.getString("success"), LangUtils.getString("success_title"),
                      JOptionPane.INFORMATION_MESSAGE);
          });
      });
    }
    catch (ClassNotFoundException | IOException e) {
      var msg = "There was a problem trying to import the data file: ";
      LOGGER.log(Level.SEVERE, msg + e.getMessage());
      UiUtils.displayErrorMessage(null, "Error", msg + "\n" + e.getMessage());
    }
  }

  /**
   * Imports all the entries in the given file.
   * 
   * @param file The file that contains the entries to import.
   * @return The list of entries.
   * @throws ClassNotFoundException If there are problems reading in the serialized objects from the file.
   * @throws IOException If there are problems reading in the file.
   */
  static List<ChamorroEntry> importEntries(File file) throws ClassNotFoundException, IOException {
    var entries = new ArrayList<ChamorroEntry>();

    try (var is = new ObjectInputStream(new FileInputStream(file))) {
      var buffer = new byte[4];
      var numBytes = is.read(buffer);
      if (numBytes != 4) {
        throw new IOException("Read only " + numBytes + " byte(s), not 4.");
      }

      if (Conversion.byteArrayToInt(buffer, 0, 0, 0, 4) != AppConstants.MAGIC_COOKIE) {
        throw new IOException("Invalid file: magic cookie does not match.");
      }

      try {
        while (true) {
          entries.add((ChamorroEntry) is.readObject());
        }
      }
      catch (EOFException e) {
        LOGGER.log(Level.INFO, "Finished importing entries from file.");
      }
    }

    return ChamorroAlphabetSorter.sort(entries);
  }

}
