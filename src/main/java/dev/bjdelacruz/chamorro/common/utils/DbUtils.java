/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.common.utils;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Utility class for retrieving database credentials.
 *
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class DbUtils {

    private DbUtils() {
    }

    private static final ResourceBundle DB_PROPERTIES_FILE;

    static {
        DB_PROPERTIES_FILE = ResourceBundle.getBundle("db", Locale.US);
    }

    /**
     * Returns the database username.
     *
     * @return The database username.
     */
    public static String getUsername() {
        return DB_PROPERTIES_FILE.getString("username");
    }

    /**
     * Returns the database password.
     *
     * @return The database password.
     */
    public static String getPassword() {
        return DB_PROPERTIES_FILE.getString("password");
    }
}
