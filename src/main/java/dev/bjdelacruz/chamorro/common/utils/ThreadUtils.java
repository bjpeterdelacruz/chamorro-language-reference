/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.common.utils;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * A utility class that contains thread-related operations.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class ThreadUtils {

  private static final Executor CACHED_EXECUTOR = Executors.newCachedThreadPool();

  /** Do not instantiate this class. */
  private ThreadUtils() {
    // Empty constructor.
  }

  /**
   * Executes the given Runnable in a new thread.
   * 
   * @param runnable The Runnable to execute.
   */
  public static void execute(Runnable runnable) {
    if (runnable == null) {
      throw new IllegalArgumentException("Runnable must not be null.");
    }
    CACHED_EXECUTOR.execute(runnable);
  }

}
