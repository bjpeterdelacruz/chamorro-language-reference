/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.common.utils;

import dev.bjdelacruz.chamorro.data.ChamorroEntry;

import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class will sort a list of Chamorro words according to the Chamorro alphabet.
 *
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class ChamorroAlphabetSorter {

    private ChamorroAlphabetSorter() {
    }

    private static final String CHAMORRO_ALPHABET = "A Å B CH D E F G H I K L M N Ñ NG O P R S T U Y";

    private static final String CHAMORRO_RULES =
            """
            < ' < a,A < å,Å < b,B < ch,Ch < d,D < e,E < f,F < g,G < h,H < i,I \
            "< k,K < l,L < m,M < n,N < ñ,Ñ < ng,Ng < o,O < p,P < r,R < s,S < t,T < u,U < y,Y < -""";
    private static final RuleBasedCollator CHAMORRO_COLLATOR;
    static {
        try {
            CHAMORRO_COLLATOR = new RuleBasedCollator(CHAMORRO_RULES);
        }
        catch (ParseException pe) {
            throw new RuntimeException(pe);
        }
    }

    /**
     * Creates an empty map of Chamorro letters.
     *
     * @return An empty map of Chamorro letters.
     */
    private static Map<String, List<ChamorroEntry>> initMap() {
        return Arrays.stream(CHAMORRO_ALPHABET.split(" ")).collect(
                        Collectors.toMap(String::toString, entry -> new ArrayList<>(), (string1, string2) -> string1, LinkedHashMap::new));
    }

    /**
     * Sorts the given list of Chamorro words. Note that a new sorted list of Chamorro words will be returned.
     *
     * @param unsortedList The list to sort.
     * @return A new sorted list.
     */
    public static List<ChamorroEntry> sort(List<ChamorroEntry> unsortedList) {
        var treeMap = initMap();

        unsortedList.forEach(entry -> {
            var word = entry.chamorro().toUpperCase(Locale.getDefault());
            if (word.startsWith("CH")) {
                treeMap.get("CH").add(entry);
            }
            else if (word.startsWith("NG")) {
                treeMap.get("NG").add(entry);
            }
            else if (entry.chamorro().startsWith("-")) {
                treeMap.get(Character.toString(word.charAt(1))).add(entry);
            }
            else {
                treeMap.get(Character.toString(word.charAt(0))).add(entry);
            }
        });

        treeMap.forEach((k, v) -> v.sort((lhs, rhs) -> CHAMORRO_COLLATOR.compare(lhs.chamorro().toUpperCase(Locale.getDefault()),
                rhs.chamorro().toUpperCase(Locale.getDefault()))));

        var sortedEntries = new ArrayList<ChamorroEntry>();
        treeMap.forEach((k, v) -> sortedEntries.addAll(v));

        return sortedEntries;
    }
}
