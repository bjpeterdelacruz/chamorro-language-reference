/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.common;

import java.awt.Font;
import java.awt.Image;
import java.nio.file.FileSystems;
import javax.swing.ImageIcon;

/**
 * This class contains constants used throughout this application.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class AppConstants {

  /** Do not instantiate this class. */
  private AppConstants() {
  }

  /** The name of this app. */
  public static final String APP_NAME = "Chamorro Language Reference";

  /** The font used in the table. */
  public static final Font TABLE_CELL_FONT = new Font("Arial", Font.PLAIN, 15);

  /** The font used in the panel that contains text fields. */
  public static final Font PANEL_FONT = new Font("Arial", Font.PLAIN, 13);

  /** The font used for the buttons. */
  public static final Font BUTTON_FONT = new Font("Arial", Font.PLAIN, 13);

  private static final String MAIN_ICON_URL = "main.png";

  private static final String ABOUT_ICON_URL = "about.png";

  /** Magic cookie for the exported data file. */
  public static final int MAGIC_COOKIE = 202_118_156;

  /** The directory in which all files relating to Chamorro Dictionary will be stored. */
  public static final String APP_DIR = System.getProperty("user.home")
      + FileSystems.getDefault().getSeparator() + "chamorro-language-reference"
      + FileSystems.getDefault().getSeparator();

  /** @return The icon used in the main window. */
  public static Image getIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(MAIN_ICON_URL)).getImage();
  }

  /** @return The icon used in the About dialog box. */
  public static ImageIcon getAboutIcon() {
    return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(ABOUT_ICON_URL));
  }

}
