/*******************************************************************************
 * Copyright (C) 2012-2024 BJ Peter Dela Cruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package dev.bjdelacruz.chamorro.common.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Utility class that contains methods for returning localized strings.
 * 
 * @author BJ Peter DeLaCruz <bj.peter.delacruz@gmail.com>
 */
public final class LangUtils {

  /** Do not instantiate this class. */
  private LangUtils() {
    // Empty constructor.
  }

  private static final ResourceBundle ENGLISH_BUNDLE;

  static {
    ENGLISH_BUNDLE = ResourceBundle.getBundle("labels", Locale.US);
  }

  /**
   * Returns a localized string for the given key using the default locale.
   * 
   * @param key The key in the properties file.
   * @return The localized string for the given key.
   */
  public static String getString(String key) {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("Key must not be null or an empty string.");
    }
    return getString(key, Locale.getDefault());
  }

  /**
   * Returns a localized string for the given key.
   * 
   * @param key The key in the properties file.
   * @param locale The locale to use to retrieve the string.
   * @return The localized string for the given key.
   */
  private static String getString(String key, Locale locale) {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("Key must not be null or an empty string.");
    }
    if (locale == null) {
      throw new IllegalArgumentException("Locale must not be null.");
    }
    var lang = locale.getLanguage();
    if ("en".equals(lang)) {
      return ENGLISH_BUNDLE.getString(key);
    }
    throw new UnsupportedOperationException("Locale not supported yet.");
  }

}
