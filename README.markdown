# Chamorro Language Reference

The world's first searchable Chamorro-English dictionary. **Over 10,000 words!**

## Downloads

macOS: [Download](https://chamorro-language-reference.s3.us-west-2.amazonaws.com/chamorro-language-reference-macos.zip)

Windows: [Download](https://chamorro-language-reference.s3.us-west-2.amazonaws.com/chamorro-language-reference-windows.zip)

## Screenshots

### macOS

![Running on macOS](https://bjdelacruz.dev/files/chamorro-language-reference-macos.png "Searching for Chamorro words and English translations that begin with fan. App is running on Mac OS X 10.10.")

### Windows 10

![Main Screen](https://bjdelacruz.dev/files/chamorro-language-reference_win10_main_screen.png "The main screen")

![Search Feature](https://bjdelacruz.dev/files/chamorro-language-reference_win10_search.png "Searching for Chamorro words and English translations that contain mag")

## User Guide

### macOS

1. Extract the `chamorro-language-reference-macos.zip` file, which can be found in the `dist` folder.

2. Open the Terminal, change into the directory to which the contents of the ZIP file were extracted, and then type `.
   /image/bin/chamorro-language-reference`.

### Windows

1. Extract the `chamorro-language-reference-windows.zip` file, which can be found in the `dist` folder.

2. Navigate to the `image\bin` directory, and then double-click on the `chamorro-language-reference.bat` file.

## Developer Guide

1. Install the following:
    * Java 23 JDK or higher

2. Run `./gradlew clean build` to compile and build a bootable JAR file.

3. To recreate the H2 database, use the RunScript tool. For example: `java -cp h2*.jar org.h2.tools.RunScript -url 
   jdbc:h2:~/data -script backup.sql`

## Links

[Developer's Website](https://bjdelacruz.dev)

### Projects

[Chamorro Language API](https://bitbucket.org/bjpeterdelacruz/chamorro-language-api/src/no-oauth2/)

[The Chamorro Dictionary](https://bitbucket.org/bjpeterdelacruz/chamorro-dictionary-web/src/develop/)

### Code Katas

* [JavaScript, CoffeeScript, TypeScript](https://bitbucket.org/bjpeterdelacruz/javascript-coffeescript-typescript-katas)
* [Kotlin](https://bitbucket.org/bjpeterdelacruz/kotlin-katas)
* [Python](https://bitbucket.org/bjpeterdelacruz/python-katas)
